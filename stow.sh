cp -f vim_dotfiles/vimrc vim/.vimrc
cp -rf vim_dotfiles/vim/ vim/.vim/
stow vim
stow spacemacs
stow zsh
stow git
